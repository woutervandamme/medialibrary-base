<?php

namespace WebSatelliet\MediaLibrary\Conversions\Events;

use Illuminate\Queue\SerializesModels;
use WebSatelliet\MediaLibrary\Conversions\Conversion;
use WebSatelliet\MediaLibrary\MediaCollections\Models\Media;

class ConversionHasBeenCompleted
{
    use SerializesModels;

    public function __construct(public Media $media, public Conversion $conversion)
    {
    }
}
