<?php

namespace WebSatelliet\MediaLibrary\MediaCollections\Exceptions;

use Exception;

abstract class FileCannotBeAdded extends Exception
{
}
