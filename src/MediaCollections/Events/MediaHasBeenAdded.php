<?php

namespace WebSatelliet\MediaLibrary\MediaCollections\Events;

use Illuminate\Queue\SerializesModels;
use WebSatelliet\MediaLibrary\MediaCollections\Models\Media;

class MediaHasBeenAdded
{
    use SerializesModels;

    public function __construct(public Media $media)
    {
    }
}
