<?php

namespace WebSatelliet\MediaLibrary\MediaCollections\Events;

use Illuminate\Queue\SerializesModels;
use WebSatelliet\MediaLibrary\HasMedia;

class CollectionHasBeenCleared
{
    use SerializesModels;

    public function __construct(public HasMedia $model, public string $collectionName)
    {
    }
}
