<?php

namespace WebSatelliet\MediaLibrary\ResponsiveImages\Events;

use Illuminate\Queue\SerializesModels;
use WebSatelliet\MediaLibrary\MediaCollections\Models\Media;

class ResponsiveImagesGenerated
{
    use SerializesModels;

    public function __construct(public Media $media)
    {
    }
}
