<?php

namespace WebSatelliet\MediaLibrary\Support;

use WebSatelliet\MediaLibrary\MediaCollections\Exceptions\FunctionalityNotAvailable;
use WebSatelliet\MediaLibraryPro\Models\TemporaryUpload;

class MediaLibraryPro
{
    public static function ensureInstalled(): void
    {
        if (! self::isInstalled()) {
            throw FunctionalityNotAvailable::mediaLibraryProRequired();
        }
    }

    public static function isInstalled(): bool
    {
        return class_exists(TemporaryUpload::class);
    }
}
